#!/usr/bin/env bash
echo "Let's start with the main framework that capsulates all other ingridients ..."
echo "Clone the Lithium PHP Framework ..."
git clone git://github.com/UnionOfRAD/framework.git
echo "Next line should get one implemented core submodule of Lithium, it's core libraries ..."
echo "`git add submodule git://github.com/UnionOfRAD/lithium.git framework/libraries/_source/lithium`"
cd framework && git submodule init && git submodule update
echo "Let's add some more fine, nifty submodules ..."
git clone git://github.com/nathansmith/formalize.git libraries/_source/formalize
git clone git://github.com/paulirish/html5-boilerplate.git libraries/_source/html5-boilerplate
git clone git://github.com/Modernizr/Modernizr.git libraries/_source/modernizr
git clone git://github.com/keithclark/selectivizr.git libraries/_source/selectivizr
git clone git://github.com/lenzcom/li3_interbase.git libraries/_source/li3_interbase
git clone git://github.com/mackstar/li3_login.git libraries/_source/li3_login
git clone git://github.com/UnionOfRAD/lithium_qa.git libraries/_source/lithium_qa
echo "Finally we create the symbolic links for our plugins"
mkdir -p libraries/li3_formalize/lib
ln -s libraries/_source/formalize libraries/li3_formalize/lib/.
mkdir -p libraries/li3_html5boilerplate/lib
ln -s libraries/_source/html5-boilerplate libraries/li3_html5boilerplate/lib/.
mkdir -p libraries/li3_modernizr/lib
ln -s libraries/_source/modernizr libraries/li3_modernizr/lib/.
mkdir -p libraries/li3_selectivizr/lib
ln -s libraries/_source/selectivizr libraries/li3_selectivizr/lib/.
echo "Last but not least some very custom libs"
ln -s libraries/_source/li3_interbase libraries/.
ln -s libraries/_source/li3_login libraries/.
ln -s libraries/_source/lithium_qa libraries/.
cd libraries/lithium_qa && git submodule init && git submodule update
echo "Finished!"
